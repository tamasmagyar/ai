import json
import pandas as pd
import seaborn as sns
import argparse
import math
import matplotlib.pyplot as plt


class AlgorithmChecker(object):

    def __init__(self, forward, backward, left, right, distance):
        self.forward = forward
        self.backward = backward * -1
        self.left = left
        self.right = right * -1
        self.max_distance = distance
        self.detected_data = None
        self.reference_data = None

    def run(self):
        self.set_data()
        print(f'F: {self.forward} B: {self.backward} L: {self.left} R:{self.right} DISTANCE: {self.max_distance}')
        self.clean_detections_data()
        self.collect_and_write_data()
        self.report()
        # self.report_generated_data()

    def clean_detections_data(self):
        """Clears empty detections from the beginning of detections.json"""
        empty_timestamps = []
        for timestamp in self.detected_data:
            if not self.detected_data[timestamp]:
                empty_timestamps.append(timestamp)
            else:
                break

        for timestamp in empty_timestamps:
            del self.detected_data[timestamp]

    def collect_and_write_data(self):
        """
        Writes all the valid data to data and area.csv.
            Valid data:
                        Distance between detected/reference objects is less than max_distance.
                        Detected and reference objects are in car's interest area.
                        Detected and reference objects' ids match.
        1st loop: Iterates through timestamps
        2nd loop: Iterates through Objects in each timestamp
        3rd loop: Checks if given detected and reference objects are valid.
        """
        results = []
        detected_positions = []
        all_objs = detected_objects = 0
        for detected_timestamp, reference_timestamp in zip(self.detected_data, self.reference_data):
            valid_data = []
            distance = None

            for ref_counter, reference_obj in enumerate(self.reference_data[reference_timestamp]):
                for detected_obj in self.detected_data[detected_timestamp]:
                    if self.check_if_valid_data(detected_obj, reference_obj):
                        distance = self.get_distance(detected_obj["position"]["x"], detected_obj["position"]["y"],
                                                     reference_obj["position"]["x"], reference_obj["position"]["y"])
                        valid_data.append((detected_obj["id"]))
                        detected_positions.append((detected_obj["id"], *self.get_x_y(detected_obj), distance))

            if valid_data:
                accuracy = len(valid_data) / ref_counter if len(valid_data) else 0
                results.append((reference_timestamp, ref_counter, len(valid_data), accuracy, distance))
                all_objs += ref_counter
                detected_objects += len(valid_data)
        self.write_results(results)
        self.write_area(detected_positions)

    def check_distance(self, det, ref):
        """:return: True if distance between @det and @ref objects is less than self.max_distance."""
        return True if self.get_distance(det[0], det[1], ref[0], ref[1]) < self.max_distance else False

    def check_if_valid_data(self, d, r):
        """
        :return: True if
                            Detected and Reference objs are inside interest distance
                            Distance between them is less then max_distance.
        """
        return True if r["id"] == d["id"] and self.check_interest(self.get_x_y(d)) and self.check_distance(
            self.get_x_y(d), self.get_x_y(r)) else False

    def check_interest(self, obj):
        """
        :param obj: Timestamp object.
        :return: True if @obj is inside of the interest area else False.
        """
        x, y = obj
        valid = True
        valid &= True if 0 <= x <= self.forward or self.backward <= x <= 0 else False
        valid &= True if 0 <= y <= self.left or self.right <= y <= 0 else False

        return valid

    def get_distance(self, det_x, det_y, ret_x, ret_y):
        """:return: Distance between det and ret timestamp objects depending on their x,y coords."""
        a = det_y - ret_y
        b = det_x - ret_x
        return math.sqrt(a ** 2 + b ** 2)

    def get_x_y(self, obj):
        """
        :param obj: Timestamp object.
        :return: x,y coordinates of @obj.
        """
        return (obj["position"]["x"], obj["position"]["y"])

    def set_data(self):
        with open("res\\reference.json") as f:
            self.reference_data = json.load(f)

        with open("res\\detections.json") as f:
            self.detected_data = json.load(f)

    def report(self, data_path="data.csv"):
        """Creates diagrams of data.csv"""

        def save_hex():
            plt.clf()
            _hex = sns.jointplot(x=data["distance"], y=data["accuracy"], kind="hex", color="#4CB391")
            _hex.savefig("report\\hex.png")

        def save_kde():
            plt.clf()
            kde = sns.jointplot(data["distance"], data["accuracy"], kind="kde", color="#4CB391")
            kde.savefig("report\\kde.png")

        def save_scatter():
            plt.clf()
            scatter = sns.scatterplot(x=data["timestamp"], y=data["accuracy"], hue=data["distance"])
            scatter.figure.savefig("report\\scatter.png")

        data = pd.read_csv(data_path)
        save_hex()
        save_kde()
        save_scatter()

    def report_generated_data(self):
        self.report("fake_data\\data.csv")

    def write_area(self, data, filename="area.csv"):
        df = pd.DataFrame.from_records(data, columns=["id", "x", "y", "distance"]).set_index("id")
        df.to_csv(filename)

    def write_results(self, data, filename="data.csv"):
        df = pd.DataFrame.from_records(data,
                                       columns=["timestamp", "total", "detected", "accuracy", "distance"]).set_index(
            "timestamp")
        df.to_csv(filename)


parser = argparse.ArgumentParser()
parser.add_argument('--forward', nargs="?", default=200)
parser.add_argument('--backward', nargs="?", default=200)
parser.add_argument('--right', nargs="?", default=200)
parser.add_argument('--left', nargs="?", default=200)
parser.add_argument('--distance', nargs="?", default=1.5)

args = parser.parse_args()
FORWARD = int(args.forward)
BACKWARD = int(args.backward)
RIGHT = int(args.right)
LEFT = int(args.left)
DISTANCE = float(args.distance)

alg_checker = AlgorithmChecker(FORWARD, BACKWARD, LEFT, RIGHT, DISTANCE)
alg_checker.run()

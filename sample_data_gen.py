import numpy as np
import pandas as pd
import scipy


def _turncnorm(N, low=0, high=25, mu=12, sigma=10):
    samples = scipy.stats.truncnorm.rvs((low - mu) / sigma, (high - mu) / sigma, loc=mu, scale=sigma, size=N)
    return samples


def gen_detected():
    data = []
    for timestamp in range(3, 5000, 4):
        _detected = abs(np.random.normal(7, 4))
        _distance = np.random.randint(0, 1000)
        data.append([timestamp, 25, round(_detected), _detected / 25])  # , _distance])
    df = pd.DataFrame.from_records(data, columns=["timestamp", "total", "detected", "accuracy"]).set_index(
        "timestamp")

    df["distance"] = _turncnorm(len(df), high=1000, mu=50, sigma=100)
    df.to_csv("fake_data\\data.csv")


gen_detected()
